<?php

$database = new PDO('mysql:host=localhost;dbname=db_smartphones;charset=UTF8', 'root', 'root');

$req = $database->prepare('SELECT * FROM t_phone NATURAL JOIN  t_manufacturer NATURAL JOIN t_exploitation');
$result = $req->execute();

$result = $req->fetchALL(PDO::FETCH_ASSOC);

?>


<!doctype html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="Mark Otto, Jacob Thornton, and Bootstrap contributors">
    <meta name="generator" content="Jekyll v3.8.5">
    <title>Cover Template · Bootstrap</title>

    <link rel="canonical" href="https://getbootstrap.com/docs/4.3/examples/cover/">

    <!-- Bootstrap core CSS -->
<link href="bootstrap.min.css" rel="stylesheet" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">


    <style>
      .bd-placeholder-img {
        font-size: 1.125rem;
        text-anchor: middle;
        -webkit-user-select: none;
        -moz-user-select: none;
        -ms-user-select: none;
        user-select: none;
      }

      @media (min-width: 768px) {
        .bd-placeholder-img-lg {
          font-size: 3.5rem;
        }
      }
    </style>
    <!-- Custom styles for this template -->
    <link href="cover.css" rel="stylesheet">
  </head>
  <body class="text-center">
    <div class="cover-container d-flex w-100 h-100 p-3 mx-auto flex-column">
  <header class="masthead mb-auto">
    <div class="inner">
      <h3 class="masthead-brand">Smartphones ranking</h3>
      <nav class="nav nav-masthead justify-content-center">
        <a class="nav-link" href="index.html">Accueil</a>
        <a class="nav-link active" href="smartphones.php">Smartphones</a>
        <a class="nav-link" href="contact.html">Contact</a>
      </nav>
    </div>
  </header>

  <main role="main" class="inner cover">

    <div>
      <table>
        <tr>
          <th>Téléphone</th>
          <th>Système d'exploitation</th>
          <th>Fabricant</th>
          <th>Taille d'écran (en pouces)</th>
          <th>Autonomie (en h)</th>
        </tr>
        <tr>
          <?php
              foreach($result as $data)
              {   
                  ?>
                  <td>
                  <?php
                    echo $data['phoName'];
                  ?>
                  </td>
                  <td>
                    <?php
                    echo $data['phoOperatingSystem'];
                    ?>
                  </td>
                  <td>
                    <?php
                    echo $data['manName'];
                    ?>
                  </td>
                  <td>
                    <?php
                    echo $data['phoScreenSize'];
                    ?>
                  </td>
                  <td>
                    <?php
                    echo $data['phoAutonomy'];
                    ?>
                  </td>
          </tr>
          <?php
          }
          ?>
        
      </table>
      </div>
  </main>

  <footer class="mastfoot mt-auto">
    <div class="inner">
      <p>ETML - École Technique des Métiers de Lausanne | Admane Monia, Berdoz Quentin, Laurella Samuel</p>
    </div>
  </footer>
</div>
</body>
</html>
