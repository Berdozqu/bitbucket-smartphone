<?php

class Database
{

    public $connector;


    public function getAllTeachers()
    {
        $req = $this->queryExecute("SELECT * FROM t_teacher");
        // Stoque les données dans une variable sous forme de tableau
        $result = $this->prepareData($req);
        // Supprime les enregistrements
        $this->closeCursor($req);
        // Retourne la variable contenant les données
        return $result;
    }


    public function __construct()
    {  
        try
        {
            $this->connector = new PDO('mysql:host=localhost;dbname=db_nickname_admanemo;charset=UTF8', 'root', 'root');
        }
        catch(PDOExeption $e)
        {
            die('erreur:'.$e->getMessage());
        }
    }


    public function queryExecute($query, $bindValues = null)
    {
        $req = $this->connector->prepare($query);
        
        if($bindValues != null)
        {
            foreach($bindValues as $array)
            {
                $req->bindValue($array['marker'], $array['var'], $array['type']);
            }
        }

        if($req->execute())
        {
            return $req;
        }

        return null;
    }


    public function prepareData($query)
    {
        // Retourne les données sous forme de tableau
        return $query->fetchALL(PDO::FETCH_ASSOC);
    }

    /**
     * 
     */
    public function closeCursor($req)
    {
        // Supprime les enregistrements
        return $req->closeCursor();
    }

    public function __destruct()
    {
        // Fermer la connexion en mettant l'objet à null
        $this->connector = null;
    }

    public function getSections()
    {
        // Execute la requête qui ira chercher le nom des sections
        $req = $this->queryExecute("SELECT * FROM t_section");
        // Stoque les données dans une variable sous forme de tableau
        $result = $this->prepareData($req);
        // Supprime les enregistrements
        $this->closeCursor($req);
        // Retourne la variable contenant les données
        return $result;
    }

    public function getTeacher($id)
    {
        $bindValues = array(
            0 => array(
                "marker" => "id",
                "var" => $id,
                "type" => PDO::PARAM_INT
            )
        );

        // Execute la requête qui ira chercher les informations d'un enseignant
        $req = $this->queryExecute("SELECT * FROM t_teacher INNER JOIN  t_section ON idSection = fkSection WHERE idTeacher = :id", $bindValues);

        // Stoque les données dans une variable sous forme de tableau
        $result = $this->prepareData($req);
        // Supprime les enregistrements
        $this->closeCursor($req);
        // Retourne la variable contenant les données
        return $result;
    }

    public function addTeacher($gender, $teacherName, $forname, $nickname, $origin, $section)
    {
        $bindValues = array(
            0 => array(
                "marker" => "gender",
                "var" => $gender,
                "type" => PDO::PARAM_STR
            ),

            1 => array(
                "marker" => "teacherName",
                "var" => $teacherName,
                "type" => PDO::PARAM_STR
            ),

            2 => array(
                "marker" => "forname",
                "var" => $forname,
                "type" => PDO::PARAM_STR
            ),

            3 => array(
                "marker" => "nickname",
                "var" => $nickname,
                "type" => PDO::PARAM_STR
            ),

            4 => array(
                "marker" => "origin",
                "var" => $origin,
                "type" => PDO::PARAM_STR
            ),

            5 => array(
                "marker" => "section",
                "var" => $section,
                "type" => PDO::PARAM_INT
            )
        );


        // Execute la requête qui ira chercher les informations d'un enseignant
        $req = $this->queryExecute("INSERT INTO t_teacher(teaGender, teaFirstname, teaName, teaNickname, teaOrigin, fkSection) VALUES (:gender, :teacherName, :forname, :nickname, :origin, :section)", $bindValues);
    }

    public function updateTeacher($gender, $teacherName, $forname, $nickname, $origin, $section, $id)
    {
        $bindValues = array(
            0 => array(
                "marker" => "gender",
                "var" => $gender,
                "type" => PDO::PARAM_STR
            ),

            1 => array(
                "marker" => "teacherName",
                "var" => $teacherName,
                "type" => PDO::PARAM_STR
            ),

            2 => array(
                "marker" => "forname",
                "var" => $forname,
                "type" => PDO::PARAM_STR
            ),

            3 => array(
                "marker" => "nickname",
                "var" => $nickname,
                "type" => PDO::PARAM_STR
            ),

            4 => array(
                "marker" => "origin",
                "var" => $origin,
                "type" => PDO::PARAM_STR
            ),

            5 => array(
                "marker" => "section",
                "var" => $section,
                "type" => PDO::PARAM_INT
            ),

            6 => array(
                "marker" => "id",
                "var" => $id,
                "type" => PDO::PARAM_INT
            ),
        );


        // Execute la requête qui ira chercher les informations d'un enseignant
        $req = $this->queryExecute("UPDATE t_teacher SET teaGender = :gender, teaFirstname = :teacherName, teaName = :forname, teaNickname = :nickname, teaOrigin = :origin, fkSection = :section WHERE idTeacher = :id", $bindValues);
    }

    function deletTeacher($id)
    {
        $bindValues = array(
            0 => array(
                "marker" => "id",
                "var" => $id,
                "type" => PDO::PARAM_INT
            )
        );

        $req = $this->queryExecute("DELETE FROM t_teacher WHERE idTeacher = :id", $bindValues);

        $this->closeCursor($req);
    }


}
?>
