SELECT idPhone, phoName, cpuFrequency, cpuCores, (cpuFrequency*cpuCores) AS CPUPerformance from t_phone 
natural join to_run 
natural join t_cpu 
ORDER BY cpuFrequency*cpuCores DESC LIMIT 10;