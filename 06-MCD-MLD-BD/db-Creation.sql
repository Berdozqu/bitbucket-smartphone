#------------------------------------------------------------
#        Script MySQL.
#------------------------------------------------------------

DROP DATABASE IF EXISTS db_smartphones;
CREATE DATABASE IF NOT EXISTS db_smartphones;
USE db_smartphones;

#------------------------------------------------------------
# Table: t_memory
#------------------------------------------------------------

CREATE TABLE t_memory(
        idMemory    TinyINT  Auto_increment  NOT NULL ,
        memCapacity TinyINT NOT NULL
	,CONSTRAINT t_memory_PK PRIMARY KEY (idMemory)
)ENGINE=InnoDB;


#------------------------------------------------------------
# Table: t_price
#------------------------------------------------------------

CREATE TABLE t_price(
        idPrice        TinyINT  Auto_increment  NOT NULL ,
        priReleased    Float NOT NULL ,
        priActualPrice Float ,
        priOneMonth    Float ,
        priSixMonth    Float
	,CONSTRAINT t_price_PK PRIMARY KEY (idPrice)
)ENGINE=InnoDB;


#------------------------------------------------------------
# Table: t_manufacturer
#------------------------------------------------------------

CREATE TABLE t_manufacturer(
        idManufacturer TinyINT  Auto_increment  NOT NULL ,
        manName        Varchar (30) NOT NULL
	,CONSTRAINT t_manufacturer_PK PRIMARY KEY (idManufacturer)
)ENGINE=InnoDB;


#------------------------------------------------------------
# Table: t_cpu
#------------------------------------------------------------

CREATE TABLE t_cpu(
        idCpu        TinyINT  Auto_increment  NOT NULL ,
        cpuFrequency Float NOT NULL ,
        cpuCores     TinyINT NOT NULL
	,CONSTRAINT t_cpu_PK PRIMARY KEY (idCpu)
)ENGINE=InnoDB;


#------------------------------------------------------------
# Table: t_exploitation
#------------------------------------------------------------

CREATE TABLE t_exploitation(
        idExploitation      TinyINT  Auto_increment  NOT NULL ,
        expOperatingSystem  Varchar (100) NOT NULL ,
        expBuilderInterface Varchar (100) NOT NULL
	,CONSTRAINT t_exploitation_PK PRIMARY KEY (idExploitation)
)ENGINE=InnoDB;


#------------------------------------------------------------
# Table: t_phone
#------------------------------------------------------------

CREATE TABLE t_phone(
        idPhone         TinyINT  Auto_increment  NOT NULL ,
        phoName         Varchar (50) NOT NULL ,
        phoScreenSize   Float NOT NULL ,
        phoAutonomy     Float NOT NULL ,
        phoReleasedDate Date NOT NULL ,
        idManufacturer  TinyINT NOT NULL ,
        idMemory        TinyINT NOT NULL ,
        idExploitation  TinyINT NOT NULL
	,CONSTRAINT t_phone_PK PRIMARY KEY (idPhone)

	,CONSTRAINT t_phone_t_manufacturer_FK FOREIGN KEY (idManufacturer) REFERENCES t_manufacturer(idManufacturer)
	,CONSTRAINT t_phone_t_memory0_FK FOREIGN KEY (idMemory) REFERENCES t_memory(idMemory)
	,CONSTRAINT t_phone_t_exploitation1_FK FOREIGN KEY (idExploitation) REFERENCES t_exploitation(idExploitation)
)ENGINE=InnoDB;


#------------------------------------------------------------
# Table: to cost
#------------------------------------------------------------

CREATE TABLE to_cost(
        idPrice TinyINT NOT NULL ,
        idPhone TinyINT NOT NULL
	,CONSTRAINT to_cost_PK PRIMARY KEY (idPrice,idPhone)

	,CONSTRAINT to_cost_t_price_FK FOREIGN KEY (idPrice) REFERENCES t_price(idPrice)
	,CONSTRAINT to_cost_t_phone0_FK FOREIGN KEY (idPhone) REFERENCES t_phone(idPhone)
)ENGINE=InnoDB;


#------------------------------------------------------------
# Table: to run
#------------------------------------------------------------

CREATE TABLE to_run(
        idCpu   TinyINT NOT NULL ,
        idPhone TinyINT NOT NULL
	,CONSTRAINT to_run_PK PRIMARY KEY (idCpu,idPhone)

	,CONSTRAINT to_run_t_cpu_FK FOREIGN KEY (idCpu) REFERENCES t_cpu(idCpu)
	,CONSTRAINT to_run_t_phone0_FK FOREIGN KEY (idPhone) REFERENCES t_phone(idPhone)
)ENGINE=InnoDB;

