-- phpMyAdmin SQL Dump
-- version 4.5.4.1
-- http://www.phpmyadmin.net
--
-- Client :  localhost
-- Généré le :  Lun 29 Avril 2019 à 08:17
-- Version du serveur :  5.7.11
-- Version de PHP :  7.0.3

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";

DROP DATABASE IF EXISTS db_smartphones;
CREATE DATABASE IF NOT EXISTS db_smartphones;
USE db_smartphones;
--
-- Base de données :  `db_smartphones`
--

-- --------------------------------------------------------

--
-- Structure de la table `to_cost`
--

CREATE TABLE `to_cost` (
  `idPrice` tinyint(4) NOT NULL,
  `idPhone` tinyint(4) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Contenu de la table `to_cost`
--

INSERT INTO `to_cost` (`idPrice`, `idPhone`) VALUES
(1, 1),
(2, 2),
(3, 3),
(4, 4),
(5, 5),
(6, 6),
(7, 7),
(8, 8),
(9, 9),
(10, 10),
(11, 11),
(12, 12),
(13, 13),
(14, 14),
(15, 15),
(16, 16),
(17, 17),
(18, 18),
(19, 19),
(20, 20),
(21, 21),
(22, 22),
(23, 23),
(24, 24),
(25, 25),
(26, 26),
(27, 27),
(28, 28),
(29, 29),
(30, 30);

-- --------------------------------------------------------

--
-- Structure de la table `to_run`
--

CREATE TABLE `to_run` (
  `idCpu` tinyint(4) NOT NULL,
  `idPhone` tinyint(4) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Contenu de la table `to_run`
--

INSERT INTO `to_run` (`idCpu`, `idPhone`) VALUES
(2, 1),
(2, 2),
(3, 3),
(29, 4),
(10, 5),
(10, 6),
(26, 7),
(21, 8),
(13, 9),
(10, 10),
(19, 11),
(12, 12),
(13, 13),
(14, 14),
(19, 15),
(26, 16),
(17, 17),
(20, 18),
(19, 19),
(20, 20),
(21, 21),
(22, 22),
(23, 23),
(24, 24),
(30, 25),
(26, 26),
(27, 27),
(29, 28),
(29, 29),
(30, 30);

-- --------------------------------------------------------

--
-- Structure de la table `t_cpu`
--

CREATE TABLE `t_cpu` (
  `idCpu` tinyint(4) NOT NULL,
  `cpuFrequency` float NOT NULL,
  `cpuCores` tinyint(4) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Contenu de la table `t_cpu`
--

INSERT INTO `t_cpu` (`idCpu`, `cpuFrequency`, `cpuCores`) VALUES
(24, 0.95, 3),
(22, 1.8, 8),
(23, 1.84, 6),
(21, 2, 8),
(30, 2.15, 4),
(3, 2.3, 4),
(29, 2.3, 8),
(13, 2.35, 8),
(26, 2.36, 8),
(27, 2.37, 4),
(2, 2.39, 6),
(10, 2.45, 8),
(14, 2.49, 8),
(17, 2.5, 6),
(12, 2.6, 8),
(20, 2.7, 8),
(19, 2.8, 8);

-- --------------------------------------------------------

--
-- Structure de la table `t_exploitation`
--

CREATE TABLE `t_exploitation` (
  `idExploitation` tinyint(4) NOT NULL,
  `expOperatingSystem` varchar(100) NOT NULL,
  `expBuilderInterface` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Contenu de la table `t_exploitation`
--

INSERT INTO `t_exploitation` (`idExploitation`, `expOperatingSystem`, `expBuilderInterface`) VALUES
(21, 'Android 6.0 Marshmallow', 'Android Stock'),
(28, 'Android 6.0 Marshmallow', 'EMUI 4'),
(25, 'Android 6.0 Marshmallow', 'MIUI 8'),
(30, 'Android 6.0.1 Marshmallow', 'Oxygen 3.1'),
(29, 'Android 6.0.1 Marshmallow', 'TouchWiz'),
(3, 'Android 7.0 Nougat', 'Experience 8.1'),
(26, 'Android 7.0 Nougat', 'MIUI 8'),
(8, 'Android 7.1 Nougat', ''),
(10, 'Android 7.1 Nougat', 'Android Stock'),
(5, 'Android 7.1 Nougat', 'Sony UX'),
(6, 'Android 7.1.1 Nougat', 'Sense'),
(9, 'Android 8.0 Oreo', 'Android Stock'),
(7, 'Android 8.0 Oreo', 'EMUI 8.0'),
(4, 'Android 8.0 Oreo', 'Experience 9.0'),
(20, 'Android 8.0 Oreo', 'Samsung Experience'),
(16, 'Android 8.1 Oreo', 'Emotion UI'),
(18, 'Android 8.1 Oreo', 'LG UX'),
(22, 'Android 8.1 Oreo', 'MIUI V9.6'),
(11, 'Android 8.1 Oreo', 'Samsung Experience'),
(13, 'Android 9.0 Pie', 'Android Stock'),
(12, 'Android 9.0 Pie', 'Emotion UI'),
(15, 'Android 9.0 Pie', 'OxygenOS'),
(19, 'Android 9.0 Pie', 'Xperia UI'),
(27, 'iOS 10', 'IOS 10'),
(2, 'IOS 11', 'IOS'),
(17, 'iOS 12', 'iOS'),
(23, 'iOS 9', 'IOS 9.3');

-- --------------------------------------------------------

--
-- Structure de la table `t_manufacturer`
--

CREATE TABLE `t_manufacturer` (
  `idManufacturer` tinyint(4) NOT NULL,
  `manName` varchar(30) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Contenu de la table `t_manufacturer`
--

INSERT INTO `t_manufacturer` (`idManufacturer`, `manName`) VALUES
(27, 'Apple'),
(8, 'BlackBerry'),
(13, 'Google'),
(28, 'Honor'),
(6, 'HTC'),
(26, 'Huawei'),
(21, 'Lenovo'),
(18, 'LG'),
(10, 'Nokia'),
(30, 'OnePlus'),
(29, 'Samsung'),
(19, 'Sony'),
(25, 'Xiaomi');

-- --------------------------------------------------------

--
-- Structure de la table `t_memory`
--

CREATE TABLE `t_memory` (
  `idMemory` tinyint(4) NOT NULL,
  `memCapacity` tinyint(4) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Contenu de la table `t_memory`
--

INSERT INTO `t_memory` (`idMemory`, `memCapacity`) VALUES
(27, 2),
(28, 3),
(29, 4),
(30, 6);

-- --------------------------------------------------------

--
-- Structure de la table `t_phone`
--

CREATE TABLE `t_phone` (
  `idPhone` tinyint(4) NOT NULL,
  `phoName` varchar(50) NOT NULL,
  `phoOperatingSystem` varchar(100) NOT NULL,
  `phoScreenSize` float NOT NULL,
  `phoAutonomy` float NOT NULL,
  `phoReleasedDate` date NOT NULL,
  `idManufacturer` tinyint(4) NOT NULL,
  `idMemory` tinyint(4) NOT NULL,
  `idExploitation` tinyint(4) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Contenu de la table `t_phone`
--

INSERT INTO `t_phone` (`idPhone`, `phoName`, `phoOperatingSystem`, `phoScreenSize`, `phoAutonomy`, `phoReleasedDate`, `idManufacturer`, `idMemory`, `idExploitation`) VALUES
(1, 'IPhone 8', 'IOS 12', 4.7, 13, '2017-09-12', 27, 27, 17),
(2, 'IPhone X', 'IOS 11', 5.8, 13, '2017-09-12', 27, 28, 2),
(3, 'Galaxy S8', 'Android 7.0 Nougat', 5.8, 11, '2017-04-21', 29, 29, 3),
(4, 'Galaxy Note8', 'Android 8.0 Oreo', 6.3, 10, '2017-09-15', 29, 30, 4),
(5, 'Xperia XZ Premium', 'Android 7.1 Nougat', 5.46, 7, '2017-06-19', 19, 29, 5),
(6, 'U11', 'Android 7.1.1 Nougat', 5.5, 10, '2018-06-23', 6, 29, 6),
(7, 'Mate 10 Pro', 'Android 8.0 Oreo', 5.9, 16, '2017-10-16', 26, 29, 7),
(8, 'KEYone', 'Android 7.1 Nougat', 4.5, 12, '2017-04-27', 8, 28, 8),
(9, 'Pixel 2 XL', 'Android 8.0 Oreo', 6, 7, '2017-10-04', 13, 29, 9),
(10, 'Nokia 8', 'Android 7.1 Nougat', 5.3, 8, '2017-09-13', 10, 29, 10),
(11, 'Galaxy Note 9', 'Android 8.1 Oreo', 6.4, 9, '2018-08-24', 29, 30, 11),
(12, 'Mate 20 Pro', 'Android 9.0 Pie', 6.39, 16, '2018-10-16', 26, 30, 12),
(13, 'Pixel 3', 'Android 9.0 Pie', 5.5, 14, '2018-11-02', 13, 29, 13),
(14, 'IPhone XS', 'IOS 12', 5.8, 14, '2018-09-21', 27, 29, 17),
(15, '6T', 'Android 9.0 Pie', 6.41, 15, '2018-11-06', 30, 30, 15),
(16, 'P20 Pro', 'Android 8.1 Oreo', 6.1, 13, '2018-03-27', 26, 30, 16),
(17, 'IPhone XR', 'iOS 12', 6.1, 15, '2018-08-19', 27, 28, 17),
(18, 'V40 Thinq', 'Android 8.1 Oreo', 6.4, 14, '2018-10-18', 18, 30, 18),
(19, 'Xperia XZ3', 'Android 9.0 Pie', 6, 11, '2018-10-05', 19, 29, 19),
(20, 'Galaxy S9', 'Android 8.0 Oreo', 5.8, 8, '2018-03-16', 29, 29, 20),
(21, 'Moto Z Play', 'Android 6.0 Marshmallow', 5.5, 19, '2018-09-01', 21, 28, 21),
(22, 'Redmi Note 6 PRO', 'Android 8.1 Oreo', 6.26, 10, '2018-10-15', 25, 28, 22),
(23, 'IPhone SE', 'iOS 9', 4, 6, '2016-03-31', 27, 27, 23),
(24, 'Galaxy A5', 'Android 6.0.1 Marshmallow', 5.2, 24, '2017-01-01', 29, 28, 29),
(25, 'Mi 5s', 'Android 6.0 Marshmallow', 5.15, 24, '2017-09-27', 25, 28, 25),
(26, 'Mate 9', 'Android 7.0 Nougat', 5.9, 26, '2016-09-01', 26, 29, 26),
(27, 'IPhone 7', 'iOS 10', 4.7, 10, '2016-09-17', 27, 27, 27),
(28, 'Honor 8', 'Android 6.0 Marshmallow', 5.2, 13, '2016-08-16', 28, 28, 28),
(29, 'Galaxy S7 Edge', 'Android 6.0.1 Marshmallow', 5.5, 13, '2016-09-04', 29, 29, 29),
(30, 'OnePlus 3', 'Android 6.0.1 Marshmallow', 5.5, 5, '2016-09-14', 30, 30, 30);

-- --------------------------------------------------------

--
-- Structure de la table `t_price`
--

CREATE TABLE `t_price` (
  `idPrice` tinyint(4) NOT NULL,
  `priReleased` float NOT NULL,
  `priActualPrice` float DEFAULT NULL,
  `priOneMonth` float DEFAULT NULL,
  `priSixMonth` float DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Contenu de la table `t_price`
--

INSERT INTO `t_price` (`idPrice`, `priReleased`, `priActualPrice`, `priOneMonth`, `priSixMonth`) VALUES
(22, 146, 129, 145, 0),
(28, 376.2, 243.276, 359.95, 345),
(25, 389.95, 159.6, 338, 208),
(24, 429, 275.4, 394, 333),
(30, 454.86, 304.38, 448.5, 429.95),
(23, 468, 379, 388.9, 317),
(21, 525.05, 429, 518.9, 517.9),
(8, 599, 504.3, 549, 556),
(15, 619, 590, 899, 0),
(10, 743.45, 539, 589, 545),
(6, 749, 379, 729, 656.15),
(27, 751.1, 459, 749.9, 679),
(7, 791, 399, 788.9, 642.4),
(26, 796.86, 311.22, 724, 615),
(5, 798.95, 399, 765.25, 683.9),
(3, 799, 444, 709.1, 599),
(29, 813.1, 478.9, 759, 633),
(19, 824.15, 689, 812.2, 701.1),
(1, 860.45, 639, 749.75, 768.3),
(16, 862, 499, 824.9, 709),
(20, 878.95, 497, 849, 599),
(17, 879, 810.8, 874.85, 0),
(13, 889, 719, 878.6, 0),
(18, 899, 846.9, 846.9, 0),
(12, 902.1, 817.9, 897.85, 0),
(11, 1036, 756, 944.1, 832.9),
(4, 1048.95, 511, 939, 779),
(2, 1195, 0, 1141, 1067),
(14, 1249, 1065.95, 1188, 0),
(9, 1319.85, 520.9, 1099, 865);

--
-- Index pour les tables exportées
--

--
-- Index pour la table `to_cost`
--
ALTER TABLE `to_cost`
  ADD PRIMARY KEY (`idPrice`,`idPhone`),
  ADD KEY `to_cost_t_phone0_FK` (`idPhone`);

--
-- Index pour la table `to_run`
--
ALTER TABLE `to_run`
  ADD PRIMARY KEY (`idCpu`,`idPhone`),
  ADD KEY `to_run_t_phone0_FK` (`idPhone`);

--
-- Index pour la table `t_cpu`
--
ALTER TABLE `t_cpu`
  ADD PRIMARY KEY (`idCpu`),
  ADD UNIQUE KEY `unique_index` (`cpuFrequency`,`cpuCores`);

--
-- Index pour la table `t_exploitation`
--
ALTER TABLE `t_exploitation`
  ADD PRIMARY KEY (`idExploitation`),
  ADD UNIQUE KEY `unique_index` (`expOperatingSystem`,`expBuilderInterface`);

--
-- Index pour la table `t_manufacturer`
--
ALTER TABLE `t_manufacturer`
  ADD PRIMARY KEY (`idManufacturer`),
  ADD UNIQUE KEY `unique_index` (`manName`);

--
-- Index pour la table `t_memory`
--
ALTER TABLE `t_memory`
  ADD PRIMARY KEY (`idMemory`),
  ADD UNIQUE KEY `unique_index` (`memCapacity`);

--
-- Index pour la table `t_phone`
--
ALTER TABLE `t_phone`
  ADD PRIMARY KEY (`idPhone`),
  ADD KEY `t_phone_t_manufacturer_FK` (`idManufacturer`),
  ADD KEY `t_phone_t_memory0_FK` (`idMemory`),
  ADD KEY `t_phone_t_exploitation1_FK` (`idExploitation`);

--
-- Index pour la table `t_price`
--
ALTER TABLE `t_price`
  ADD PRIMARY KEY (`idPrice`),
  ADD UNIQUE KEY `unique_index` (`priReleased`,`priActualPrice`,`priOneMonth`,`priSixMonth`);

--
-- AUTO_INCREMENT pour les tables exportées
--

--
-- AUTO_INCREMENT pour la table `t_cpu`
--
ALTER TABLE `t_cpu`
  MODIFY `idCpu` tinyint(4) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=32;
--
-- AUTO_INCREMENT pour la table `t_exploitation`
--
ALTER TABLE `t_exploitation`
  MODIFY `idExploitation` tinyint(4) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=32;
--
-- AUTO_INCREMENT pour la table `t_manufacturer`
--
ALTER TABLE `t_manufacturer`
  MODIFY `idManufacturer` tinyint(4) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=32;
--
-- AUTO_INCREMENT pour la table `t_memory`
--
ALTER TABLE `t_memory`
  MODIFY `idMemory` tinyint(4) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=32;
--
-- AUTO_INCREMENT pour la table `t_phone`
--
ALTER TABLE `t_phone`
  MODIFY `idPhone` tinyint(4) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=32;
--
-- AUTO_INCREMENT pour la table `t_price`
--
ALTER TABLE `t_price`
  MODIFY `idPrice` tinyint(4) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=32;
--
-- Contraintes pour les tables exportées
--

--
-- Contraintes pour la table `to_cost`
--
ALTER TABLE `to_cost`
  ADD CONSTRAINT `to_cost_t_phone0_FK` FOREIGN KEY (`idPhone`) REFERENCES `t_phone` (`idPhone`),
  ADD CONSTRAINT `to_cost_t_price_FK` FOREIGN KEY (`idPrice`) REFERENCES `t_price` (`idPrice`);

--
-- Contraintes pour la table `to_run`
--
ALTER TABLE `to_run`
  ADD CONSTRAINT `to_run_t_cpu_FK` FOREIGN KEY (`idCpu`) REFERENCES `t_cpu` (`idCpu`),
  ADD CONSTRAINT `to_run_t_phone0_FK` FOREIGN KEY (`idPhone`) REFERENCES `t_phone` (`idPhone`);

--
-- Contraintes pour la table `t_phone`
--
ALTER TABLE `t_phone`
  ADD CONSTRAINT `t_phone_t_exploitation1_FK` FOREIGN KEY (`idExploitation`) REFERENCES `t_exploitation` (`idExploitation`),
  ADD CONSTRAINT `t_phone_t_manufacturer_FK` FOREIGN KEY (`idManufacturer`) REFERENCES `t_manufacturer` (`idManufacturer`),
  ADD CONSTRAINT `t_phone_t_memory0_FK` FOREIGN KEY (`idMemory`) REFERENCES `t_memory` (`idMemory`);
